package com.official.todolist;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class DataBaseManager extends SQLiteOpenHelper {
    private static final String DATABASE_NAME ="Todo.db";
    private static final int DATABASE_VERSION = 7;

    public DataBaseManager(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    String strSqlT = "create table Tache ( idT INTEGER primary key autoincrement, nomT String, descT String, dateLimiteT String, dateAjoutT String)";
    String strSqlC = "create table Color ( idC INTEGER primary key autoincrement, nomC String, hexa String)";
    db.execSQL(strSqlT);
    db.execSQL(strSqlC);

    String strSqlColor1 = "insert into Color (nomC, hexa) values('Non_Urgente', '0')";
    String strSqlColor2 = "insert into Color (nomC, hexa) values('Urgente', '0')";
    String strSqlColor3 = "insert into Color (nomC, hexa) values('Tres_Urgente', '0')";
    String strSqlColor4 = "insert into Color (nomC, hexa) values('Critique', '0')";

    db.execSQL(strSqlColor1);
    db.execSQL(strSqlColor2);
    db.execSQL(strSqlColor3);
    db.execSQL(strSqlColor4);

    Log.i("DATABASE","OnCreate invoqué");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String strSqlT ="drop table Tache";
        String strSqlC ="drop table Color";
        db.execSQL(strSqlT);
        db.execSQL(strSqlC);
        this.onCreate(db);
    }
    public ArrayList<Tache> lectureTache(){
        ArrayList<Tache>taches = new ArrayList<>();
        String sqlT = "select idT, nomT, descT, dateLimiteT, dateAjoutT from Tache";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlT,null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast()){
            Tache tache = new Tache(curseur.getInt(0),curseur.getString(1),curseur.getString(2),curseur.getString(3),curseur.getString(4));
            taches.add(tache);
            curseur.moveToNext();
        }
        curseur.close();
        return taches;
    }
    public void insertTache(String nomT, String descT,String dateLimiteT, String dateAjoutT){
        String strSql = "insert into Tache (nomT,descT,dateLimiteT,dateAjoutT) values('"+nomT+"','"+descT+"','"+dateLimiteT+"','"+dateAjoutT+"')";
        this.getWritableDatabase().execSQL(strSql);

    }
    public void suppTache(int idT){
        String strSqlsupp = "Delete from Tache where idT = "+idT;
        this.getWritableDatabase().execSQL(strSqlsupp);
    }

    public void modifTache(int pidT, String pnomT, String pdescT,String pdateT){
        String strSqlmodif = " UPDATE Tache SET nomT = '" + pnomT + "', descT = '" + pdescT + "', dateLimiteT = '" + pdateT + "' WHERE idT = " + pidT;
        this.getWritableDatabase().execSQL(strSqlmodif);
    }

    public ArrayList<Couleur> lectureCouleur(){
        ArrayList<Couleur> couleurs = new ArrayList<>();
        String sqlC = "select * from Color";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlC, null);
        curseur.moveToFirst();
        while (!curseur.isAfterLast()){
            Couleur color = new Couleur(curseur.getInt(0), curseur.getString(1), curseur.getString(2));
            couleurs.add(color);
            curseur.moveToNext();
        }
        curseur.close();
        return couleurs;
    }

    public void modifColor(int pidC, String pHexa){
        String strSqlUpdateColor = "UPDATE Color SET hexa = '" + pHexa + "' WHERE idC = " + pidC;
        this.getWritableDatabase().execSQL(strSqlUpdateColor);
    }

}
