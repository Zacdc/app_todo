package com.official.todolist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ArrayList<Tache> listeTache;
    DataBaseManager dbm;
    ListView lvListe;
    Button BtAdd;
    Button BtColor;
    Button BtMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvListe = (ListView) findViewById(R.id.lvTache);
        listeTache = new DataBaseManager(this).lectureTache();

        BtAdd = (Button) findViewById(R.id.btNewTache);
        BtAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,AjoutTache.class);
                startActivity(intent);
            }
        });

        BtColor = (Button) findViewById(R.id.btPersoCouleur);
        BtColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,PersonnalisationCouleur.class);
                startActivity(intent);
            }
        });

        BtMode = (Button) findViewById(R.id.btModeNuit);
        BtMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        });


        lvListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startGestionActivity(position);
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        ListeAdapter listeAdapter = new ListeAdapter(this,listeTache);
        lvListe.setAdapter(listeAdapter);

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Toast.makeText(this, sdf.format(date.getTime()),Toast.LENGTH_LONG).show();
    }

    private void startGestionActivity(int i){
        Tache uneTache = listeTache.get(i);
        Intent intent = new Intent(this, GestionActivity.class);
        intent.putExtra("idT", uneTache.getIdT());
        intent.putExtra("nomT", uneTache.getNomT());
        intent.putExtra("descT", uneTache.getDescT());
        intent.putExtra("dateT", uneTache.getDateT());
        startActivity(intent);
    }

}