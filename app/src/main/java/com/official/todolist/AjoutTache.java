package com.official.todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AjoutTache extends AppCompatActivity {

    private DataBaseManager dbm;

    EditText nomT;
    EditText descT;
    EditText dateT;
    Button btAjout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_tache);

        nomT = (EditText) findViewById(R.id.TitreNewTache);
        descT = (EditText) findViewById(R.id.DescNewTache);
        dateT = (EditText) findViewById(R.id.NewDate);

        btAjout = (Button)findViewById(R.id.btAjout);
        dbm = new DataBaseManager((this));

        btAjout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                String DateAjout = formatter.format(date);

                String nomTache = nomT.getText().toString();
                String descTache = descT.getText().toString();
                String dateTache = dateT.getText().toString();

                if (nomTache.matches("") || descTache.matches("") || dateTache.matches("")){
                    Toast.makeText(getApplicationContext(), "Vous devez renseigner tous les champs !", Toast.LENGTH_LONG).show();
                }
                else {
                    dbm.insertTache(nomT.getText().toString(), descT.getText().toString(), dateT.getText().toString(), DateAjout);
                    Intent intent = new Intent(AjoutTache.this,MainActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}