package com.official.todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.util.ArrayList;

import yuku.ambilwarna.AmbilWarnaDialog;

public class PersonnalisationCouleur extends AppCompatActivity {

    DataBaseManager dbm = new DataBaseManager(this);
    ArrayList<Couleur> listeCouleurs;

    Button btNonUrgent;
    Button btUrgent;
    Button btTresUrgent;
    Button btCritique;
    Button btValider;

    int color1;
    int color2;
    int color3;
    int color4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personnalisation_couleur);

        listeCouleurs = dbm.lectureCouleur();
        final String colorNonUrgent = listeCouleurs.get(0).hexa;
        String colorUrgent = listeCouleurs.get(1).hexa;
        String colorTresUrgent = listeCouleurs.get(2).hexa;
        String colorCritique = listeCouleurs.get(3).hexa;

        btNonUrgent = (Button) findViewById(R.id.btNonUrgent);
        btUrgent = (Button) findViewById(R.id.btUrgent);
        btTresUrgent = (Button) findViewById(R.id.btTresUrgent);
        btCritique = (Button) findViewById(R.id.btCritique);

        btNonUrgent.setBackgroundColor(Integer.parseInt(colorNonUrgent));
        btUrgent.setBackgroundColor(Integer.parseInt(colorUrgent));
        btTresUrgent.setBackgroundColor(Integer.parseInt(colorTresUrgent));
        btCritique.setBackgroundColor(Integer.parseInt(colorCritique));

        color1 = Integer.parseInt(colorNonUrgent);
        color2 = Integer.parseInt(colorUrgent);
        color3 = Integer.parseInt(colorTresUrgent);
        color4 = Integer.parseInt(colorCritique);

        btNonUrgent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openColorPicker(1);
            }
        });
        btUrgent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openColorPicker(2);
            }
        });
        btTresUrgent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openColorPicker(3);
            }
        });
        btCritique.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openColorPicker(4);
            }
        });

        btValider = (Button) findViewById(R.id.btValiderColor);
        btValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbm.modifColor(1, String.valueOf(color1));
                dbm.modifColor(2, String.valueOf(color2));
                dbm.modifColor(3, String.valueOf(color3));
                dbm.modifColor(4, String.valueOf(color4));

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void openColorPicker(int i){
        if (i == 1){
            AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(this, color1, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                @Override
                public void onCancel(AmbilWarnaDialog dialog) {

                }

                @Override
                public void onOk(AmbilWarnaDialog dialog, int color) {
                    color1 = color;
                    btNonUrgent.setBackgroundColor(color1);
                }
            });
            colorPicker.show();
        }
        if (i == 2){
            AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(this, color2, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                @Override
                public void onCancel(AmbilWarnaDialog dialog) {

                }

                @Override
                public void onOk(AmbilWarnaDialog dialog, int color) {
                    color2 = color;
                    btUrgent.setBackgroundColor(color2);
                }
            });
            colorPicker.show();
        }
        if (i == 3 ){
            AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(this, color3, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                @Override
                public void onCancel(AmbilWarnaDialog dialog) {

                }

                @Override
                public void onOk(AmbilWarnaDialog dialog, int color) {
                    color3 = color;
                    btTresUrgent.setBackgroundColor(color3);
                }
            });
            colorPicker.show();
        }
        if (i == 4){
            AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(this, color4, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                @Override
                public void onCancel(AmbilWarnaDialog dialog) {

                }

                @Override
                public void onOk(AmbilWarnaDialog dialog, int color) {
                    color4 = color;
                    btCritique.setBackgroundColor(color4);
                }
            });
            colorPicker.show();
        }
    }
}