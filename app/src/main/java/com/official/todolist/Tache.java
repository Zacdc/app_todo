package com.official.todolist;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Tache {
    int idT;
    String NomT;
    String DescT;
    String DateLimiteT;

    String DateAjoutT;

    public Tache(int idT, String nomt, String desct, String dateLimiteT, String dateAjoutT) {
        this.idT = idT;
        NomT = nomt;
        DescT = desct;
        DateLimiteT = dateLimiteT;
        DateAjoutT = dateAjoutT;
    }

    public int getIdT() {
        return idT;
    }

    public void setIdT(int idT) {
        this.idT = idT;
    }

    public String getNomT() {
        return NomT;
    }

    public void setNomT(String nomT) {
        NomT = nomT;
    }

    public String getDescT() {
        return DescT;
    }

    public void setDescT(String descT) {
        DescT = descT;
    }

    public String getDateT() {
        return DateLimiteT;
    }

    public void setDateT(String dateT) {
        DateLimiteT = dateT;
    }

    public String getDateAjoutT() { return DateAjoutT; }

    public void setDateAjoutT(String dateAjoutT) { DateAjoutT = dateAjoutT; }


    public static Comparator<Tache> ComparatorDate = new Comparator<Tache>() {
        @Override
        public int compare(Tache o1, Tache o2) {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            java.util.Date date = new java.util.Date();
            java.util.Date aujourdhui = null;
            try {
                aujourdhui = sdf.parse(sdf.format(date.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            java.util.Date dateLimite = null;
            try {
                dateLimite = sdf.parse(o1.getDateT());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date dateAjout = null;
            try {
                dateAjout = sdf.parse(o1.getDateAjoutT());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long diffInMillies = Math.abs(dateLimite.getTime() - aujourdhui.getTime());
            long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

            long diffInMillies2 = Math.abs(dateLimite.getTime() - dateAjout.getTime());
            long diff2 = TimeUnit.DAYS.convert(diffInMillies2, TimeUnit.MILLISECONDS);

            return 0;
        }
    };
}
