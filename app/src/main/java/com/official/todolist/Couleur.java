package com.official.todolist;

public class Couleur {

    int idC;
    String nomC;
    String hexa;

    public Couleur(int pidC, String pnomC, String phexa ){
        idC = pidC;
        nomC = pnomC;
        hexa = phexa;
    }

    public int getIdC() {
        return idC;
    }

    public void setIdC(int idC) {
        this.idC = idC;
    }

    public String getNomC() {
        return nomC;
    }

    public void setNomC(String nomC) {
        this.nomC = nomC;
    }

    public String getHexa() {
        return hexa;
    }

    public void setHexa(String hexa) {
        this.hexa = hexa;
    }
}
