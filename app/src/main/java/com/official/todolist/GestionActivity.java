package com.official.todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class GestionActivity extends AppCompatActivity {
    private DataBaseManager dbm;

    int idTache;
    EditText nomT;
    EditText descT;
    EditText dateT;
    Button btModif;
    Button btSuppr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion);

        Intent intent = getIntent();

        nomT = (EditText) findViewById(R.id.TitreNewTache);
        descT = (EditText) findViewById(R.id.DescNewTache);
        dateT = (EditText) findViewById(R.id.NewDate);

        nomT.setText(intent.getStringExtra("nomT"));
        descT.setText(intent.getStringExtra("descT"));
        dateT.setText(intent.getStringExtra("dateT"));

        idTache = intent.getIntExtra("idT", 0);

        btModif = (Button) findViewById(R.id.btModif);
        btSuppr = (Button) findViewById(R.id.btSupp);

        dbm = new DataBaseManager((this));

        if (idTache == 0){
            Toast.makeText(this,"La tache n'a pas pu être modifiée (probleme d'id)" ,Toast.LENGTH_LONG).show();
        }

        btModif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbm.modifTache(idTache, nomT.getText().toString() ,descT.getText().toString() ,dateT.getText().toString());
                Intent intent = new Intent(GestionActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });

        btSuppr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbm.suppTache(idTache);
                Intent intent = new Intent(GestionActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }
}