package com.official.todolist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ListeAdapter extends ArrayAdapter<Tache> {
    Context context;
    DataBaseManager dbm = new DataBaseManager(getContext());
    TextView nomT;
    TextView descT;
    TextView dateT;
    TextView idT;
    LinearLayout idLinear;

    ArrayList<Couleur> listeCouleurs;

    public ListeAdapter(Context context, List<Tache> listeTache){
        super(context, -1,listeTache);
        this.context = context;
    }
    @SuppressLint("ResourceAsColor")
    @RequiresApi(api = Build.VERSION_CODES.O)
    public View getView(int position, View convertiView, ViewGroup parent) {
        View view;
        Tache uneTache;
        view = null;

        if (convertiView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.ligne, parent, false);
        } else {
            view = convertiView;
        }

        uneTache = getItem(position);
        nomT = (TextView) view.findViewById(R.id.tvTitre);
        descT = (TextView) view.findViewById(R.id.tvDescription);
        dateT = (TextView) view.findViewById(R.id.tvDate);
        idT = (TextView) view.findViewById(R.id.tvIdTache);
        idLinear = (LinearLayout) view.findViewById(R.id.linearTache);

        nomT.setText(uneTache.getNomT());
        descT.setText(uneTache.getDescT());
        dateT.setText(uneTache.getDateT());
        idT.setText(Integer.toString(uneTache.getIdT()));

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        Date date = new Date();
        Date aujourdhui = null;
        try {
            aujourdhui = sdf.parse(sdf.format(date.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date dateLimite = null;
        try {
            dateLimite = sdf.parse(uneTache.getDateT());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date dateAjout = null;
        try {
            dateAjout = sdf.parse(uneTache.getDateAjoutT());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diffInMillies = Math.abs(dateLimite.getTime() - aujourdhui.getTime()
        );
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

        long diffInMillies2 = Math.abs(dateLimite.getTime() - dateAjout.getTime());
        long diff2 = TimeUnit.DAYS.convert(diffInMillies2, TimeUnit.MILLISECONDS);

        listeCouleurs = dbm.lectureCouleur();
        String colorNonUrgent = listeCouleurs.get(0).hexa;
        String colorUrgent = listeCouleurs.get(1).hexa;
        String colorTresUrgent = listeCouleurs.get(2).hexa;
        String colorCritique = listeCouleurs.get(3).hexa;

        if ( diff >= Math.ceil((diff2*75)/100)){
            //idLinear.setBackgroundResource(R.color.colorPrioVert);
            idLinear.setBackgroundColor(Integer.parseInt(colorNonUrgent));
        }
        if (Math.ceil((diff2*75)/100) > diff && diff >= Math.ceil((diff2*50)/100)){
            //idLinear.setBackgroundResource(R.color.colorPrioJaune);
            idLinear.setBackgroundColor(Integer.parseInt(colorUrgent));
        }
        if (Math.ceil((diff2*50)/100) > diff && diff >= Math.ceil((diff2*25)/100)){
            //idLinear.setBackgroundResource(R.color.colorPrioOrange);
            idLinear.setBackgroundColor(Integer.parseInt(colorTresUrgent));
        }
        if (Math.ceil((diff2*25)/100) > diff){
            //idLinear.setBackgroundResource(R.color.colorPrioRouge);
            idLinear.setBackgroundColor(Integer.parseInt(colorCritique));
        }
        return view;
    }
}
